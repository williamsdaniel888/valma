#!/usr/bin/env python

import sys
import argparse
import os, os.path
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import av
from torchvision import transforms
from PIL import Image

file_extensions = ['.mov','.mp4']

transform = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])

torch.hub._validate_not_a_forked_repo=lambda a,b,c: True
vggnet = torch.hub.load('pytorch/vision:v0.10.0', 'vgg11', pretrained=True)


class AutoEncoder(nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()

        """ ARCHITECTURE 

        # Encoder (LSTM)
        # Encoder Vector (take the final hidden state from the encoder RNN)
        # Decoder (LSTM)

        """
        self.encoder_RNN1 = nn.LSTM(input_size=1000,hidden_size=1000,num_layers=1,batch_first=True,dropout=0)
        # Feature vector size is 1000
        self.decoder_RNN1 = nn.LSTM(input_size=1000,hidden_size=1000,num_layers=1,batch_first=True,dropout=0)

    def forward(self, x):

        """
        Parameters
        ----------
        input_tensor:
            5-D Tensor of shape (b, t, c, h, w)        #   batch, time, channel, height, width
        """
        y = x.reshape(-1,1,1000) # Flatten to 1D
        y = self.encoder_RNN1(y)[0] # Extract the encoder RNN's output
        
        z = self.decoder_RNN1(y)[0] # Extract the decoder RNN's output
        z = z.squeeze()

        return (y[-1],z) # Return a tuple with the final encoder output and the autoencoder's output

# Get the video frames and convert them to numeric tensors
def read_video(path):
    stills = []
    with av.open(path) as container:
        stream = container.streams.video[0]
        stream.codec_context.skip_frame = 'NONKEY' # Get key frames
        for frame in container.decode(stream):
            if len(stills)<100: # Truncate at 100 key frames
                frame = frame.to_rgb().to_ndarray().astype('uint8')
                frame = Image.fromarray(frame)
                frame = np.array(transform(frame))
                stills.append(frame)
        # data = np.transpose(np.array(stills),(0,3,1,2)) # Rearrange the tensor to be (time,color, height, width)
        data = np.array(stills).copy()
    return data # np.flipud(data) #for time-reversing the video frames

# Define a PyTorch dataset class
class VideoDataset(Dataset):
    # Initialize the dataset using a folder's contents
    def __init__(self, vid_dir):
        self.vid_dir = vid_dir
        self.listdir = [name for name in os.listdir(self.vid_dir) if os.path.isfile(self.vid_dir+name) and name[-4:].lower() in file_extensions]
    
    # Find the dataset length
    def __len__(self):
        return len(self.listdir)  

    # Get an item from the dataset
    def __getitem__(self, idx):
        vid_path = os.path.join(self.vid_dir,self.listdir[idx])
        return torch.from_numpy(read_video(vid_path))

# Run the model training
def train(train_path,val_path,output_name):
    # Create dataset objects and dataloaders for the training and validation sets
    train_data = VideoDataset(train_path)
    train_dataloader = DataLoader(train_data, batch_size=1, shuffle=True)
    val_data = VideoDataset(val_path)
    val_dataloader = DataLoader(val_data, batch_size=1, shuffle=True)

    # Model instantiation and hyperparameter setup
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("Using {} for computation.".format(device))
    model = AutoEncoder()
    model.to(device)
    vggnet.to(device)

    # Training hyperparameters
    learning_rate = 0.001   
    lossfn = nn.MSELoss()
    optim = torch.optim.Adam(model.parameters(), lr=learning_rate)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optim, 'min')
    
    # Model training
    max_epochs = 30
    trainloss = np.zeros((max_epochs))
    validloss = np.zeros((max_epochs))
    worst_val_loss = 9e99 # initialize the worst validation loss

    # Loop over epochs
    for epoch in range(max_epochs):
        optim.zero_grad() # Reset the optimizer's gradient tracker

        # Training
        model.train()
        for n, local_batch in enumerate(train_dataloader):
            local_batch = local_batch.squeeze().to(device) # Transfer input to GPU
            with torch.no_grad():
                y = vggnet(local_batch)
            loss_tr = lossfn(model(y)[1], y) # Compute loss
            loss_tr.backward() # Perform optimization update
            optim.step() # Step the learning rate
            optim.zero_grad() # Reset gradient tracker

        # Validation
        model.eval()
        with torch.no_grad():
            for n,local_batch in enumerate(val_dataloader):
                local_batch = local_batch.squeeze().to(device) # Transfer input to GPU
                with torch.no_grad():
                    y = vggnet(local_batch)
                loss_v = lossfn(model(y)[1], y) # Compute loss
            scheduler.step(loss_v)

            # Record the training and validation losses, display a status update
            trainloss[epoch] = loss_tr.item()
            validloss[epoch] = loss_v.item()
            print('After epoch #{:03d}\tTrain Loss: {:.4f}\tVal Loss: {:.4f}'.format(epoch+1, trainloss[epoch], validloss[epoch]),end='')
            
            if validloss[epoch]<worst_val_loss: # Validation loss is the lowest yet
                print("*")
                torch.save(model.state_dict(), "./{}".format(output_name)) # Save final model parameters
                worst_val_loss = validloss[epoch] # Update the validation loss tracker
            else:
                print('')
    return 1

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='train_valma',description='Test')
    parser.add_argument('--train_path', action='store', help='Path to training set folder', default='./train/')
    parser.add_argument('--val_path', action='store', help='Path to validation set folder', default='./val/')
    parser.add_argument('--output_name', action='store', help='Name the output model file')
    args = parser.parse_args()
    train_path = args.train_path
    val_path = args.val_path
    output_name = args.output_name
    train(train_path,val_path,output_name)