#!/usr/bin/env python

import sys
import argparse
import os
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import av
from torchvision import transforms
from PIL import Image

file_extensions = ['.mov','.mp4']

transform = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])

torch.hub._validate_not_a_forked_repo=lambda a,b,c: True
vggnet = torch.hub.load('pytorch/vision:v0.10.0', 'vgg11', pretrained=True)


class AutoEncoder(nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()

        """ ARCHITECTURE 

        # Encoder (LSTM)
        # Encoder Vector (take the final hidden state from the encoder RNN)
        # Decoder (LSTM)

        """
        self.encoder_RNN1 = nn.LSTM(input_size=1000,hidden_size=1000,num_layers=1,batch_first=True,dropout=0)
        # Feature vector size is 1000
        self.decoder_RNN1 = nn.LSTM(input_size=1000,hidden_size=1000,num_layers=1,batch_first=True,dropout=0)

    def forward(self, x):

        """
        Parameters
        ----------
        input_tensor:
            5-D Tensor of shape (b, t, c, h, w)        #   batch, time, channel, height, width
        """
        y = x.reshape(-1,1,1000) # Flatten to 1D
        y = self.encoder_RNN1(y)[0] # Extract the encoder RNN's output
        
        z = self.decoder_RNN1(y)[0] # Extract the decoder RNN's output
        z = z.squeeze()

        return (y[-1],z) # Return a tuple with the final encoder output and the autoencoder's output

# Get the video frames and convert them to numeric tensors
def read_video(path):
    stills = []
    with av.open(path) as container:
        stream = container.streams.video[0]
        stream.codec_context.skip_frame = 'NONKEY' # Get key frames
        for frame in container.decode(stream):
            if len(stills)<100: # Truncate at 100 key frames
                frame = frame.to_rgb().to_ndarray().astype('uint8')
                frame = Image.fromarray(frame)
                frame = np.array(transform(frame))
                stills.append(frame)
        # data = np.transpose(np.array(stills),(0,3,1,2)) # Rearrange the tensor to be (time,color, height, width)
    return np.array(stills)

# Get the feature vector for a specific video and save it to a CSV file
def getVideoFeatures(path,model_path,output_name):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    zin = torch.from_numpy(read_video(input_path))
    model = AutoEncoder()
    model.load_state_dict(torch.load(model_path, map_location=device))
    with torch.no_grad():
        zin = vggnet(zin)
        x = model(zin)[0].detach().squeeze().numpy()
    df = pd.DataFrame(x)
    df.to_csv(output_name,index=False)
    return x

# Get the feature vectors for videos in a folder and save them to a CSV file
def getVideoFeaturesFolder(input_path,model_path,output_name):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = AutoEncoder()
    model.load_state_dict(torch.load(model_path, map_location=device))
    film_paths = [os.path.join(input_path,name) for name in os.listdir(input_path) if (os.path.isfile(input_path+name) and name[-4:].lower() in file_extensions)]
    output = np.empty((0,model.encoder_RNN1.hidden_size))
    for i in film_paths:
        zin = torch.from_numpy(read_video(i))
        with torch.no_grad():
            zin = vggnet(zin)
            output = np.vstack((output,model(zin)[0].detach().squeeze().numpy()))
    x = pd.DataFrame(output)
    x.to_csv(output_name,index=False)
    return x

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='getFeatures',description='Test')
    parser.add_argument('--input_path', action='store', help='Path to the raw input file/folder', default='./inputs/')
    parser.add_argument('--model_path', action='store', help='Path to the model file', default='./model_valma.pt')
    parser.add_argument('--output_name', action='store', help='Name of the output .csv file', default='features_valma.csv')
    args = parser.parse_args()
    input_path = args.input_path
    model_path = args.model_path
    output_name = args.output_name
    if input_path[-4:].lower() in file_extensions:
        x = getVideoFeatures(input_path,model_path,output_name)
        print(x)
    else:
        x = getVideoFeaturesFolder(input_path,model_path,output_name)
        print(x)