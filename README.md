# Valma: Visual and Longitudinal Motion Autoencoder

Contents:
- [How to Use the Code](#how-to-use-the-code)
    - [Prerequisites](#prerequisites)
    - [Training Valma](#training-Valma)
    - [Inference with Valma](#inference-with-valma)
- [Implementation Details](#implementation-details)
    - [Background](#background)
    - [Model Architecture](#model-architecture): [data pre-processing](#data-pre-processing), [encoder](#encoder), [decoder](#decoder).
    - [Hyperparameters and training setup](#hyperparameters-and-training-setup).
    - [Future Work](#future-work)
    - [References](#references)

## How to Use the Code

### Prerequisites

Please run the command `pip install -r requirements.txt` to install all necessary prerequisite packages.

### Training Valma

1. Get the data for training and validation and place them in separate folders.
1. Run the training script with `python train_Valma.py --train_path=[train_path] --val_path=[val_path] --output_name=[output_name]`.
    1. `train_path`: a path to the training set of videos or pose data.
        1. Default: `./train/`
        1. **Note:**  train_valma.py currently handles `.mov` and `.mp4` files, but with refactoring should be able to work with any 1080p RGB videos in formats recognized by `FFMPEG`. train_tab.py currently handles `.mat` files.
    1. `val_path`: a path to the validation set of videos or pose data.
        1. Default: `./val/`
        1. **Note:**  train_valma.py currently handles `.mov` and `.mp4` files, but with refactoring should be able to work with any 1080p RGB videos in formats recognized by `FFMPEG`. train_tab.py currently handles `.mat` files.
    1. `output_name`: the name of the PyTorch model file to save for later inference. :bangbang: **Caution! Make a backup and carefully specify the name of the output model file to avoid overwriting any existing models in the directory!** :bangbang:

### Inference with Valma

1. Get the input data; it's recommended that you place them in a folder `./inputs/`.
1. Run the inference script with `python getFeatures_Valma.py --input_path=[input_path] --model_path=[model_path] --output_name=[output_name]`. 
    1. `input_path`: a path to an input item or a folder containing the raw input items.
        1. Default: `./inputs/`
        1. **Note:**  getFeatures_valma.py currently handles `.mov` and `.mp4` files, but with refactoring should work with any 1080p RGB videos in formats recognized by `FFMPEG`. getFeatures_tab.py currently handles `.mat` files.
    1. `model_path`: the path to the previously-trained model file.
        1. Default: `model_valma.pt`
    1. `output_name`: the name of the output file containing the feature vector(s).
        1. Default: `features_valma.csv`

## Implementation Details


### Background

Variational auto-encoders form a special class of machine learning models that seek to generate an output identical to the input. The model contains two components: an encoder and a decoder. In between the two components, there is a data bottleneck, with the encoded input projected into a lower-dimensional hidden space. By training the auto-encoder, we can learn a mapping from the input data space to the hidden space. In this way we can extract a compact representation of complex data inputs. We have constructed an auto-encoder in order to extract a unique feature vector $\Upsilon$ from video frames containing robot swarm movements. 

This approach has been motivated by a perceived gap in the literature for systematically describing the evolution of robot swarm behaviors. While video summarization algorithms can extract a sequence of key frames from videos [\[1\]](#jadon2019video), the output is also time series data. For downstream processing, it can be more convenient to compact the time series into a single tensor (cf. the use of recurrent neural networks in NLP for sentence summarization). An auto-encoder can perform this conversion, with the advantage that the training process is unsupervised. An auto-encoder is explored in [\[2\]](#aberman_learning_2019) and [\[3\]](#aberman_unpaired_2020), however the considered model focuses on the motion characteristics of human figures, so it only handles pose data generated from human physiques. Our auto-encoder does not assume a geometry for swarm agents *a priori* and does not require explicit pose data for swarm agents as input.

### Model Architecture

![valma](valma.png)

*Figure 1: Valma Model Overview*

An overview of the model's subsystems is depicted in Figure 1. 
The model contains a pre-processing component (extracting frame features using the `VGGNet-A` computer vision model [\[4\]](#simonyan_very_2014)) and two additional recurrent neural network components: an encoder and a decoder. 
The encoder processes the sequence of frame features, calculating a feature vector x<sub>i</sub> &isin; ℝ<sup>1000</sup> at each time step i. 
The decoder attempts the inverse operation, that is, to predict the sequence of frame features given the sequence of feature vectors \{x<sub>1</sub>,x<sub>2</sub>,x<sub>3</sub>...\}.

#### Data Pre-processing

We first use the `PyAV` library to extract key frames from a video file and then convert the sequence of frames into a four-dimensional tensor of shape (N,3,1080,1920), representing time step **n**, the pixel RGB color channel **c**, pixel height **h**, and pixel width **w**. 
For training we truncate the videos to 100 key frames due to GPU memory limitations. 
We then perform a series of frame transformations (rescaling to (3,256,256), cropping to (3,240,240), and [normalizing the frames](https://pytorch.org/hub/pytorch_vision_vgg/)), and pass each transformed frame through `VGGNet-A`. 
This yields a 1000-dimensional feature vector **x**<sub>i</sub> for each frame.

#### Encoder

At each time step, the encoder reads the 1000-dimensional feature vector. 
This is fed into a single-layer long short-term memory (LSTM) network, mapping to a latent feature space. 
Note that we define the output vector at the final time step as the video's feature vector, i.e. **x**:=**x**<sub>N</sub>.

#### Decoder

The decoder feeds **x**<sub>i</sub> to another single-layer LSTM network, generating a 1000-dimensional output vector at each time step. 
At its output, the decoder attempts to reconstruct the original input to the encoder. 

### Hyperparameters and Training Setup
The choices of model hyperparameters for each component in the encoder and decoder are summarized in the table below. Stochastic gradient descent used mini-batches of size 1, an initial learning rate of 0.001 and the Adam optimization algorithm.


**Subsystem** | **Component** | **Parameter** | **Value**
:------:|:------:|:------:|:------:
Encoder |  LSTM<sub>1</sub>  | Input Size | 1000
" |  " | Output Size | 1000
" |  "  | Number of Layers | 1
" |  "  | Dropout | 0
Decoder |  LSTM<sub>2</sub>  | Input Size | 1000
" |  "  | Output Size | 1000
" |  "  | Number of Layers | 1
" |  "  | Dropout | 0

### Future Work

In many dimensions there is relatively little difference between feature vectors for distinct videos, hence we have standardized the feature vectors using the training set's mean and standard deviation vectors. 
We speculate that this is due to the relatively small size of the training set when compared with the space of all possible video inputs. 
This might be mitigated by using a larger and more diverse set of training videos. 
In addition, the truncation of feature vectors to 20 dimensions was motivated by computational constraints; the influence of feature vector truncation on SVM performance should be investigated further.
Aligning video frames with motion-captured pose data may also enhance model performance.
Finally, further optimization of hyperparameters should be conducted, particularly using non-zero dropout with the LSTM layers, and larger mini-batch sizes. 

### References

<a name="jadon2019video">1</a>. S. Jadon and M. Jasim, “Video summarization using keyframe extraction and video skimming,” *arXiv preprint [arXiv:1910.04792](https://arxiv.org/abs/1910.04792)*, 2019.

<a name="aberman_learning_2019">2</a>. K.  Aberman,  R.  Wu,  D.  Lischinski,  B.  Chen,  and  D.  Cohen-Or,  “Learning  character-agnostic  motionfor motion retargeting in 2D,” *ACM Transactions on Graphics*, vol. 38, no. 4, pp. 75:1–75:14, Jul. 2019. [Online]. Available:  https://doi.org/10.1145/3306346.3322999

<a name="aberman_unpaired_2020">3</a>. K. Aberman, Y. Weng, D. Lischinski, D. Cohen-Or, and B. Chen, “Unpaired motion style transfer fromvideo  to  animation,” *ACM Transactions on Graphics*,  vol.  39,  no.  4,  pp.  64:64:1–64:64:12,  Jul.  2020. [Online]. Available:  https://doi.org/10.1145/3386569.3392469

<a name="simonyan_very_2014">4</a>. K. Simonyan and A. Zisserman, “Very Deep Convolutional Networks for Large-Scale Image Recognition,” Sep. 2014. *arXiv preprint [arXiv:1409.1556v6](https://arxiv.org/abs/1409.1556v6)*, 2014.